<?php

function getLastProductId($IdCategoria) {
    /**
     * @var $conn mysqli
     *
     * (int) --> Cast a INT
     *
     * %$ricerca% -->Cerca qualsiasi cosa contenga quello che ho cercato.
     *
     */
    $idProdotto = '';
    $conn = $GLOBALS['mysqli'];

    $sql = "SELECT MAX(PRD_ID) FROM TSML08_PRD";
    $sql .= "WHERE CTG_CD = $IdCategoria";
    $res = $conn->query($sql);

    if ($res) {
        $idProdotto = $res->fetch_assoc();
    }

    return $idProdotto;
}
