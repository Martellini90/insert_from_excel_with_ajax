<?php   
	 //Ricordarsi di ABILITARE:
	 //extension=php_openssl.dll	
            use PHPMailer\PHPMailer\PHPMailer;
            use PHPMailer\PHPMailer\SMTP;
            use PHPMailer\PHPMailer\Exception;

	function InviaMail($Mittente,$Oggetto,$TestoMessaggio) {
            //File necessario ad usare la funzione PHPMailer ()
            require 'PHPMailer/src/Exception.php';
            require 'PHPMailer/src/PHPMailer.php';
            require 'PHPMailer/src/SMTP.php';
			
            //Imposto utilizzo della funzione PHPMailer ()
			$mail = new PHPMailer();  
			
            //Imposto lingua in cui verranno mostrati eventuali messaggi de errore
            $mail->setLanguage("it", 'PHPMailer/language/phpmailer.lang-it.php');
        
			$mail->isSMTP();//isSMTP indica alla funzione PHPMailer () di utilizzare il protocollo del server SMTP
	 
	/*****************************N:B:***************************************
			(->)Questo simbolo si legge
			
			SMTPDebug Serve allo sviluppatore per visualizzare gli errori
			
			0 = off 
			1 = client messages
			2 = client and server messages
	************************************************************************/
	 
			$mail->SMTPDebug = 2;
	
	/*****************************N:B:***************************************
		Porte 587 e 465 sono per le Connessioni in uscita CIFRATE
		Porta 25 Connessioni in uscita NON CIFRATE
	************************************************************************/
	
			$mail->Port = Porta;
	 
	/*****************************N.B:**************************************
		TLS -->Utilizza la porta 587; CIFRATURA MASSIMA
		SSL -->Utilizza la porta 465; CIFRATURA MEDIA
		' ' -->Utilizza la porta 25; NESSUNA CIFRATURA
		
		SSL è l'assegnazione standard
	************************************************************************/
    
			$mail->SMTPSecure = SMPT;//Metodo di autenticazione
	
	/*****************************N:B:***************************************
		true--->Server richiede autenticazione (username & password)
		false-->Autenticazione non richiesta
	************************************************************************/
 
			$mail->SMTPAuth = true;
 
			$mail->Host = HostMail;//Server della posta in uscita
	 
			//Dati di accesso all'account mail del mittente (username e PW)
			$mail->Username = Username;
			$mail->Password = Password;
     
	/*****************************N:B:***************************************
		Imposta il mittente:
			-Indirizzo di Posta
			-Nome da Visualizzare
	************************************************************************/         
	        //Mittente -->Indirizzo da cui sarà spedita la mail
			//$mail->setFrom(Mittente, NomeVisualizzato);
			$mail->setFrom($Mittente);
			
		//Rispondi A:(Imposta Indirizzo di risposta )
			//$mail->addReplyTo(Mittente, 'Paolino Paperino');
			 
		//Destinatario A:
            $mail->addAddress(Destinatario);
	/***************Testo della Mail*************
		Defisco una variabile che contenga il testo della mail
	    
        N.B:
		Eventuali regole di stile per il testo del messaggio devono essere inserite nella mail utilizzando il tag <style>. 
	********************************************/
								
			$mail->Subject = Oggetto;
			$mail->msgHTML($TestoMessaggio);
			
			$bOk = $mail->send();//Eseguo Funzione di Invio

            $result = array (
                'success'       => true,
                'error'         => '',
            );

			if(!$bOk)
				{
					echo('<pre>'.$mail->ErrorInfo.'</pre>');
				}
        
            else{
				echo 'Mail inviata';
            }


			 
			return $bOk;
	}



