function leggiExcel (event){
    let selectedFile = event.target.files[0];
    if (selectedFile) {
        var fileReader = new FileReader();

        fileReader.onload = function(event) {
            var data = event.target.result;

            var workbook = XLSX.read(data, {
                type: "binary"
            });
            workbook.SheetNames.forEach(sheet => {
                let rowObject = XLSX.utils.sheet_to_row_object_array(
                    workbook.Sheets[sheet]
                );

                let jsonObject = JSON.stringify(rowObject);
                // document.getElementById("jsonData").innerHTML = jsonObject;
                console.log(jsonObject);
                $.ajax({
                    async:false,
                    type: "POST",
                    url: './php/validazione.php',
                    dataType: 'json',
                    data: {
                        jsonData: jsonObject,
                    },
                    success: function () {
                        console.log('chiamata Ajax effettuata')
                    },
                    error: function () {
                        alert("Chiamata fallita, si prega di riprovare...")
                    }
                })
            });
        };
        fileReader.readAsBinaryString(selectedFile);
    }
}

